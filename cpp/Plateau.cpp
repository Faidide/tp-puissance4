#include "../hpp/Plateau.hpp"

#include <algorithm>
#include <cmath>


// Constructeur avec la taille par défaut
Plateau::Plateau()
  : tour(JOUEUR1),
    hauteur_max(0),
    largeur(7),
    hauteur(6),
    nombre_a_la_suite(4) {
  try {
    // cree la grile comme un tableau de taille par defaut
    grille = new uint8_t[42];
    // hauteur_remplissement pour chaque ligne
    hauteur_remplissement = new int[7]();
    // Initialisation a la valeur de VIDE pour toute la zone memoire de grille
    memset(grille, VIDE, largeur * hauteur);
    // remplis la grille haute de zéros
    for (int i = 0; i < 7; i++) {
      hauteur_remplissement[i] = 0;
    }
  } catch (std::bad_alloc& erreur) {
    std::cerr << "Erreur lors de l'allocation de la grille." << std::endl;
    // passe l'erreur au contexte de l'appelleur
    throw;
  }
}

// Constructeur avec une taille customizable
Plateau::Plateau(int Largeur, int Hauteur)
  : tour(JOUEUR1),
    hauteur_max(0),
    largeur(Largeur),
    hauteur(Hauteur),
    nombre_a_la_suite(4) {
  // vérification de la taille
  if (Largeur < 4 || Hauteur < 4) {
    throw std::runtime_error("Taille de plateau invalide");
  }
  if (Largeur > DIM_MAX || Hauteur > DIM_MAX) {
    throw std::runtime_error("Taille de plateau invalide");
  }
  try {
    grille = new uint8_t[Largeur * Hauteur];
    // Initialisation a 0 de la zone memoire pour le tableau
    // hauteur_remplissement
    hauteur_remplissement = new int[Largeur]();
    // Initialisation a la valeur de VIDE pour toute la zone memoire de grille
    memset(grille, VIDE, Largeur * Hauteur);
    // remplis la grille haute de zéros
    for (int i = 0; i < Largeur; i++) {
      hauteur_remplissement[i] = 0;
    }
  } catch (std::bad_alloc& erreur) {
    std::cerr << "Erreur lors de l'allocation de la grille." << std::endl;
    // passe l'erreur au contexte de l'appelleur
    throw;
  }
}

// Custom avec taille et nombre de jeton pour gagner customizables
Plateau::Plateau(int Largeur, int Hauteur, int nb_a_la_suite)
  : tour(JOUEUR1),
    hauteur_max(0),
    largeur(Largeur),
    hauteur(Hauteur),
    nombre_a_la_suite(nb_a_la_suite) {
  // vérification de la taille
  if (nb_a_la_suite < 2) {
    throw std::runtime_error("Taille de combinaison gagnante invalide");
  }
  // vérification de la taille
  if (Largeur < nb_a_la_suite || Hauteur < nb_a_la_suite) {
    throw std::runtime_error("Taille de plateau invalide");
  }
  if (Largeur > DIM_MAX || Hauteur > DIM_MAX || nb_a_la_suite > Largeur ||
      nb_a_la_suite > Hauteur) {
    throw std::runtime_error("Taille de plateau invalide");
  }
  // allocations
  try {
    grille = new uint8_t[Largeur * Hauteur];
    // Initialisation a 0 de la zone memoire pour le tableau
    // hauteur_remplissement
    hauteur_remplissement = new int[Largeur]();
    // Initialisation a la valeur de VIDE pour toute la zone memoire de grille
    memset(grille, VIDE, Largeur * Hauteur);
    // remplis la grille haute de zéros
    for (int i = 0; i < Largeur; i++) {
      hauteur_remplissement[i] = 0;
    }
  } catch (std::bad_alloc& erreur) {
    std::cerr << "Erreur lors de l'allocation de la grille." << std::endl;
    // passe l'erreur au contexte de l'appelleur
    throw;
  }
}

// On desalloue la memoire alloue dans le constructeur lors de la destruction de
// l'objet
Plateau::~Plateau() {
  delete[] grille;
  delete[] hauteur_remplissement;
}

bool Plateau::dansGrille(int colonne, int ligne) const {
  return ligne >= 0 && ligne < hauteur && colonne >= 0 && colonne < largeur;
}

// Setteur de la grille, on place le jeton(=VIDE ou ROUGE ou JAUNE) a l'endroit
// precise
void Plateau::setGrille(int colonne, int ligne, uint8_t jeton) {
  if (!dansGrille(colonne, ligne)) {
    std::cerr << "Tentative d'accès à une case invalide !" << std::endl;
    throw std::runtime_error("Accès à une case invalide.");
  }
  if (jeton != VIDE && jeton != ROUGE && jeton != JAUNE) {
    throw std::runtime_error("Identifiant de jeton invalide");
  }
  grille[(largeur * ligne) + colonne] = jeton;

  // On incremente la hauteur de la grille sur la colonne colonne et on met a
  // jour la hauteur (en partant du bas) max de notre grille
  if (jeton != VIDE) {
    hauteur_remplissement[colonne]++;
    if (hauteur_remplissement[colonne] > hauteur_max)
      hauteur_max = hauteur_remplissement[colonne];
  }
}

// Renvoie la valeur de la grille a la ligne ligne et colonne colonne
uint8_t Plateau::getGrille(int colonne, int ligne) const {
  if (!dansGrille(colonne, ligne)) {
    std::cerr << "Tentative d'accès à une case invalide !" << std::endl;
    throw std::runtime_error("Accès à une case invalide.");
  }
  return grille[(largeur * ligne) + colonne];
}

// affichage de notre grille
void Plateau::affiche() const {
  for (int ligne = 0; ligne < hauteur; ligne++) {
    for (int colonne = 0; colonne < largeur; colonne++) {
      if (getGrille(colonne, ligne) == VIDE)
        std::cout << "- ";
      else if (getGrille(colonne, ligne) == ROUGE)
        std::cout << "R ";
      else
        std::cout << "J ";
    }
    std::cout << std::endl;
  }
  std::cout << std::endl;
}

// modifie le plateau pour que le joueur courant ait joue dans la colonne
// colonne, retourne false si erreur true sinon
bool Plateau::joue(int colonne) {
  if (colonne < 0 || colonne >= largeur) {
    return false;
  }
  // One ne peut jouer sur une colonne deja pleine
  if (hauteur_remplissement[colonne] == hauteur) return false;

  if (tour == JOUEUR1) {
    setGrille(colonne, hauteur - hauteur_remplissement[colonne] - 1, JOUEUR1);
    tour = JOUEUR2;
  } else {
    setGrille(colonne, hauteur - hauteur_remplissement[colonne] - 1, JOUEUR2);
    tour = JOUEUR1;
  }

  return true;
}

// test si on peut jouer sans rien modifier, renvoie la hauteur (-1 si
// impossible)
int Plateau::joueTest(int colonne) {
  if (colonne < 0 || colonne >= largeur) {
    return -1;
  }
  // One ne peut jouer sur une colonne deja pleine
  if (hauteur_remplissement[colonne] == hauteur) return -1;

  return hauteur_remplissement[colonne];
}

// Verification de gain pour le joueur joueur dans la grille de P pour les
// suites horizontales
bool Plateau::gagne_horizontale(uint8_t joueur) const {
  // vérification de la validité du paramètre joueur
  if (joueur != JOUEUR1 && joueur != JOUEUR2) {
    throw std::runtime_error("Identifiant de joueur incorrect.");
  }

  // pour chaque ligne
  for (int i = 0; i < (int)hauteur; i++) {
    // on va parcourir les colonnes et chercher si une chaine fait gagner
    int plusLongueLigne = 0;
    for (int j = 0; j < (int)largeur; j++) {
      // récup le jeton à la case
      if (getGrille(j, i) == joueur) {
        plusLongueLigne++;
      } else {
        plusLongueLigne = 0;
      }
      // vérifie la gagne
      if (plusLongueLigne >= (int)nombre_a_la_suite) return true;
    }
  }
  // if we reach here, nothing was found
  return false;
}

bool Plateau::gagne_verticale(uint8_t joueur) const {
  // vérification de la validité du paramètre joueur
  if (joueur != JOUEUR1 && joueur != JOUEUR2) {
    throw std::runtime_error("Identifiant de joueur incorrect.");
  }

  // pour chaque colonne
  for (int i = 0; i < (int)largeur; i++) {
    // on va parcourir la ligne et chercher si une chaine fait gagner
    int plusLongueLigne = 0;
    for (int j = 0; j < (int)hauteur; j++) {
      // récup le jeton à la case
      if (getGrille(i, j) == joueur) {
        plusLongueLigne++;
      } else {
        plusLongueLigne = 0;
      }
      // vérifie la gagne
      if (plusLongueLigne >= (int)nombre_a_la_suite) return true;
    }
  }
  // si on arrive ici, rien n'a été trouvé
  return false;
}

bool Plateau::gagne_diagonale(uint8_t joueur) const {
  // verifie les diagonales qui partent de la ligne du haut vers la droite
  // pour chaque case i de la ligne du haut
  for (int i = 0; i < largeur; i++) {
    // initialise le nombre de pas
    int nb_pas = 0;
    // initialise le nombre de pions successifs
    int pion_successifs = 0;
    // tant que la case i+nb_pas,nb_pas est dans le plateau
    while (dansGrille(i + nb_pas, nb_pas)) {
      // si la case i+nb_pas,nb_pas est un jeton joueur
      if (getGrille(i + nb_pas, nb_pas) == joueur) {
        // incrementer le nombre de pions successifs
        pion_successifs++;
        // verifie si il y a gagne
        if (pion_successifs >= nombre_a_la_suite) {
          return true;
        }
      } else {
        // sinon, remettre ce nombre a zero
        pion_successifs = 0;
      }
      // incrémentation du pas
      nb_pas++;
    }
  }

  // verifie les diagonales qui partent de la ligne de gauche vers la droite
  // pour chaque case i de la ligne du haut
  for (int i = 0; i < hauteur; i++) {
    // initialise le nombre de pas
    int nb_pas = 0;
    // initialise le nombre de pions successifs
    int pion_successifs = 0;
    // tant que la case i+nb_pas,nb_pas est dans le plateau
    while (dansGrille(nb_pas, i + nb_pas)) {
      // si la case i+nb_pas,nb_pas est un jeton joueur
      if (getGrille(nb_pas, i + nb_pas) == joueur) {
        // incrementer le nombre de pions successifs
        pion_successifs++;
        // verifie si il y a gagne
        if (pion_successifs >= nombre_a_la_suite) {
          return true;
        }
      } else {
        // sinon, remettre ce nombre a zero
        pion_successifs = 0;
      }
      // incrémentation du pas
      nb_pas++;
    }
  }

  // verifie les diagonales qui partent de la ligne du haut vers la gauche
  // pour chaque case i de la ligne du haut
  for (int i = 0; i < largeur; i++) {
    // initialise le nombre de pas
    int nb_pas = 0;
    // initialise le nombre de pions successifs
    int pion_successifs = 0;
    // tant que la case i+nb_pas,largeur-1-nb_pas est dans le plateau
    while (dansGrille(i - nb_pas, nb_pas)) {
      // si la case i+nb_pas,nb_pas est un jeton joueur
      if (getGrille(i - nb_pas, nb_pas) == joueur) {
        // incrementer le nombre de pions successifs
        pion_successifs++;
        // verifie si il y a gagne
        if (pion_successifs >= nombre_a_la_suite) {
          return true;
        }
      } else {
        // sinon, remettre ce nombre a zero
        pion_successifs = 0;
      }
      // incrémentation du pas
      nb_pas++;
    }
  }

  // verifie les diagonales qui partent de la ligne de droite vers la gauche
  // pour chaque case i de la ligne de droite
  for (int i = 0; i < hauteur; i++) {
    // initialise le nombre de pas
    int nb_pas = 0;
    // initialise le nombre de pions successifs
    int pion_successifs = 0;
    // tant que la case i+nb_pas,nb_pas est dans le plateau
    while (dansGrille(largeur - 1 - nb_pas, i + nb_pas)) {
      // si la case i+nb_pas,nb_pas est un jeton joueur
      if (getGrille(largeur - 1 - nb_pas, i + nb_pas) == joueur) {
        // incrementer le nombre de pions successifs
        pion_successifs++;
        // verifie si il y a gagne
        if (pion_successifs >= nombre_a_la_suite) {
          return true;
        }
      } else {
        // sinon, remettre ce nombre a zero
        pion_successifs = 0;
      }
      // incrémentation du pas
      nb_pas++;
    }
  }

  // si on arrive ici, le joueur n'a pas gagné
  return false;
}

// indique si le joueur passe en argument a gagne
bool Plateau::gagne(uint8_t joueur) const {
  return (gagne_verticale(joueur) || gagne_horizontale(joueur) ||
          gagne_diagonale(joueur));
}

int Plateau::getLargeur() const { return largeur; }
int Plateau::getHauteur() const { return hauteur; }

uint8_t Plateau::getTour() const { return tour; }

// retire un jeton du plateau
bool Plateau::retireJeton(int colonne) {
  if (colonne < 0 || colonne >= largeur) {
    return false;
  }
  // Si la ligne est vide, ne rien faire
  if (hauteur_remplissement[colonne] == 0) return false;

  // sinon, retirer le jeton et décrémenter la hauteur
  setGrille(colonne, hauteur - hauteur_remplissement[colonne], VIDE);

  // on met à jour la hauteur de la colonne de jetons
  hauteur_remplissement[colonne]--;

  // on change le joueur à qui c'est le tour
  if (tour == JOUEUR1) {
    tour = JOUEUR2;
  } else {
    tour = JOUEUR1;
  }

  return true;
}

// calcule le score du plateau pour le joueur
int Plateau::score(uint8_t joueur) const {
  // le score est la somme des scores en différentes directions
  int score =
    scoreHorizontale(joueur) + scoreVerticale(joueur) + scoreDiagonale(joueur);
  // auquel on soustrait le score de l'autre joueur
  uint8_t autreJoueur;
  if (joueur == JOUEUR2)
    autreJoueur = JOUEUR1;
  else
    autreJoueur = JOUEUR2;
  int scoreAutre = scoreHorizontale(autreJoueur) + scoreVerticale(autreJoueur) +
                   scoreDiagonale(autreJoueur);
  return (score - scoreAutre);
}

// calcule le score du joueur à l'horizontale
int Plateau::scoreHorizontale(uint8_t joueur) const {
  // vérification de la validité du paramètre joueur
  if (joueur != JOUEUR1 && joueur != JOUEUR2) {
    throw std::runtime_error("Identifiant de joueur incorrect.");
  }

  int maxScore = 0;

  // pour chaque ligne
  for (int i = 0; i < hauteur; i++) {
    // on va parcourir les colonnes et chercher si une chaine fait gagner
    int plusLongueLigne = 0;
    for (int j = 0; j < largeur; j++) {
      // récup le jeton à la case
      if (getGrille(j, i) == joueur) {
        plusLongueLigne++;
      } else {
        plusLongueLigne = 0;
      }
      // vérifie la gagne
      if (plusLongueLigne >= nombre_a_la_suite) return 100000;
      // si la combinaison est la meilleure
      if (plusLongueLigne > maxScore) {
        // tester si elle a la place d'être complétée
        //if (ligneEstTerminableHorizontalement(j, i, joueur)) {
        maxScore = plusLongueLigne;
        //}
      }
    }
  }
  // si le score est à un pion de la victoire, renvoyer le double de score
  int multiplier = 1;
  if (maxScore + 1 == nombre_a_la_suite) multiplier = 20;
  // calcule le score
  return maxScore*100*multiplier;
}

// calcule si une ligne peut être terminée en vérifiant les côtés
bool Plateau::ligneEstTerminableHorizontalement(int x, int y,
                                                uint8_t joueur) const {
  // le potentiel de la ligne qui commence à la case x,y
  int potentiel = 1;
  // aller voir à droite
  int xCurseur = x + 1;
  while (xCurseur < largeur &&
         (getGrille(xCurseur, y) == joueur || getGrille(xCurseur, y) == VIDE)) {
    potentiel++;
    xCurseur++;
  }
  // aller voir à gauche
  xCurseur = x - 1;
  while (xCurseur > 0 &&
         (getGrille(xCurseur, y) == joueur || getGrille(xCurseur, y) == VIDE)) {
    potentiel++;
    xCurseur--;
  }
  // test final
  return potentiel >= nombre_a_la_suite;
}

// calcule si une ligne peut être terminée en vérifiant les côtés
bool Plateau::ligneEstTerminableVerticalement(int x, int y,
                                              uint8_t joueur) const {
  // le potentiel de la ligne qui commence à la case x,y
  int potentiel = 1;
  // aller voir à droite
  int yCurseur = y + 1;
  while (yCurseur < hauteur &&
         (getGrille(x, yCurseur) == joueur || getGrille(x, yCurseur) == VIDE)) {
    potentiel++;
    yCurseur++;
  }
  // aller voir à gauche
  yCurseur = y - 1;
  while (yCurseur > 0 &&
         (getGrille(x, yCurseur) == joueur || getGrille(x, yCurseur) == VIDE)) {
    potentiel++;
    yCurseur--;
  }
  // test final
  return potentiel >= nombre_a_la_suite;
}

// calcule le score du joueur à la verticale
int Plateau::scoreVerticale(uint8_t joueur) const {
  if (joueur != JOUEUR1 && joueur != JOUEUR2) {
    throw std::runtime_error("Identifiant de joueur incorrect.");
  }

  int maxScore = 0;

  // pour chaque colonne
  for (int i = 0; i < (int)largeur; i++) {
    // on va parcourir la ligne et chercher si une chaine fait gagner
    int plusLongueLigne = 0;
    for (int j = 0; j < (int)hauteur; j++) {
      // récup le jeton à la case
      if (getGrille(i, j) == joueur) {
        plusLongueLigne++;
      } else {
        plusLongueLigne = 0;
      }
      // vérifie la gagne
      if (plusLongueLigne >= (int)nombre_a_la_suite) return 100000;
      // si la combinaison est la meilleure
      if (plusLongueLigne > maxScore) {
        // tester si elle a la place d'être complétée
        //if (ligneEstTerminableVerticalement(i, j, joueur)) {
        maxScore = plusLongueLigne;
        //}
      }
    }
  }

  // si le score est à un pion de la victoire, renvoyer le double de score
  int multiplier = 1;
  if (maxScore + 1 == nombre_a_la_suite) multiplier = 20;
  // calcule le score
  return maxScore*100*multiplier;
}

// calcule le score du joueur à la diagonale
int Plateau::scoreDiagonale(uint8_t joueur) const {
  if (joueur != JOUEUR1 && joueur != JOUEUR2) {
    throw std::runtime_error("Identifiant de joueur incorrect.");
  }

  int maxScore = 0;

  // verifie les diagonales qui partent de la ligne du haut vers la droite
  // pour chaque case i de la ligne du haut
  for (int i = 0; i < largeur; i++) {
    // initialise le nombre de pas
    int nb_pas = 0;
    // initialise le nombre de pions successifs
    int pion_successifs = 0;
    // tant que la case i+nb_pas,nb_pas est dans le plateau
    while (dansGrille(i + nb_pas, nb_pas)) {
      // si la case i+nb_pas,nb_pas est un jeton joueur
      if (getGrille(i + nb_pas, nb_pas) == joueur) {
        // incrementer le nombre de pions successifs
        pion_successifs++;
        // verifie si il y a gagne
        if (pion_successifs >= nombre_a_la_suite) {
          return 100000;
        }
        maxScore = std::max(pion_successifs, maxScore);
      } else {
        // sinon, remettre ce nombre a zero
        pion_successifs = 0;
      }
      // incrémentation du pas
      nb_pas++;
    }
  }

  // verifie les diagonales qui partent de la ligne de gauche vers la droite
  // pour chaque case i de la ligne du haut
  for (int i = 0; i < hauteur; i++) {
    // initialise le nombre de pas
    int nb_pas = 0;
    // initialise le nombre de pions successifs
    int pion_successifs = 0;
    // tant que la case i+nb_pas,nb_pas est dans le plateau
    while (dansGrille(nb_pas, i + nb_pas)) {
      // si la case i+nb_pas,nb_pas est un jeton joueur
      if (getGrille(nb_pas, i + nb_pas) == joueur) {
        // incrementer le nombre de pions successifs
        pion_successifs++;
        // verifie si il y a gagne
        if (pion_successifs >= nombre_a_la_suite) {
          return 100000;
        }
        maxScore = std::max(pion_successifs, maxScore);
      } else {
        // sinon, remettre ce nombre a zero
        pion_successifs = 0;
      }
      // incrémentation du pas
      nb_pas++;
    }
  }

  // verifie les diagonales qui partent de la ligne du haut vers la gauche
  // pour chaque case i de la ligne du haut
  for (int i = 0; i < largeur; i++) {
    // initialise le nombre de pas
    int nb_pas = 0;
    // initialise le nombre de pions successifs
    int pion_successifs = 0;
    // tant que la case i+nb_pas,largeur-1-nb_pas est dans le plateau
    while (dansGrille(i - nb_pas, nb_pas)) {
      // si la case i+nb_pas,nb_pas est un jeton joueur
      if (getGrille(i - nb_pas, nb_pas) == joueur) {
        // incrementer le nombre de pions successifs
        pion_successifs++;
        // verifie si il y a gagne
        if (pion_successifs >= nombre_a_la_suite) {
          return 100000;
        }
        maxScore = std::max(pion_successifs, maxScore);
      } else {
        // sinon, remettre ce nombre a zero
        pion_successifs = 0;
      }
      // incrémentation du pas
      nb_pas++;
    }
  }

  // verifie les diagonales qui partent de la ligne de droite vers la gauche
  // pour chaque case i de la ligne de droite
  for (int i = 0; i < hauteur; i++) {
    // initialise le nombre de pas
    int nb_pas = 0;
    // initialise le nombre de pions successifs
    int pion_successifs = 0;
    // tant que la case i+nb_pas,nb_pas est dans le plateau
    while (dansGrille(largeur - 1 - nb_pas, i + nb_pas)) {
      // si la case i+nb_pas,nb_pas est un jeton joueur
      if (getGrille(largeur - 1 - nb_pas, i + nb_pas) == joueur) {
        // incrementer le nombre de pions successifs
        pion_successifs++;
        // verifie si il y a gagne
        if (pion_successifs >= nombre_a_la_suite) {
          return 100000;
        }
        maxScore = std::max(pion_successifs, maxScore);
      } else {
        // sinon, remettre ce nombre a zero
        pion_successifs = 0;
      }
      // incrémentation du pas
      nb_pas++;
    }
  }

  // si le score est à un pion de la victoire, renvoyer le double de score
  int multiplier = 1;
  if (maxScore + 1 == nombre_a_la_suite) multiplier = 2;
  // calcule le score
  return (((int)std::floor((float)maxScore / (float)nombre_a_la_suite) * 10)) *
         multiplier;
}

int Plateau::nbCasesVides() const {
  // nombre totale de cases
  int restantes = getLargeur()*getHauteur();
  // pour chaque case
  for(int i=0;i<getLargeur();i++) {
    for(int j=0;j<getHauteur();j++) {
      // si elle est occupée, enveler 1 à restantes
      if(getGrille(i,j)!=VIDE) {
        restantes -= 1;
      }
    }
  }

  return restantes;
}