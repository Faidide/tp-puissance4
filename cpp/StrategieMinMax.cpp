#include "../hpp/StrategieMinMax.hpp"

#include <cmath>

// Constructors/Destructors
//

StrategieMinMax::StrategieMinMax(int profondeur)
  : profondeur_minmax(profondeur) {}

StrategieMinMax::StrategieMinMax() : profondeur_minmax(4) {}

/**
 * Choisis une colonne sur laquelle jouer.
 * @return int
 * @param  partie Plateau de jeu sur lequel ou souhaite jouer.
 */
int StrategieMinMax::choix(Plateau* partie) {
  // applique l'algorithme
  ScoreStruct res = minimax(profondeur_minmax, true, partie);
  int colonne = res.cases_jouees % partie->getLargeur();
  // si la strat est en pls à cause d'une fin de plateau
  if (partie->joueTest(colonne) == -1 || res.score == -1000000 ||
      res.score == 1000000 || res.score == INVALID_FORECAST) {
    // tester toutes les casses unes à une
    for (int i = 0; partie->getLargeur(); i++) {
      // si la case est jouable, la jouer
      if (partie->joueTest(i) != -1) {
        return i;
      }
    }
  }
  // retourne la colonne sur laquelle jouer si valide
  return colonne;
}

StrategieMinMax::~StrategieMinMax() {}

/**
 * minimax algorithm
 *
 * @param profondeur : un entier qui décrit la profondeur à atteindre restante
 * @param joueur2 : le booleen qui dit que c'est le tour du joueur2
 * @param plateau : le pointeur vers le plateau de jeu
 * @param cases_base_largeur : un entier long qui encode
 * en base largeur les choix à appliquer au plateau pour calculer le score
 * @param niveau : le niveau de profondeur atteint pour le moment, sert ajouter
 * à cases_base_largeur
 *
 * @return scorestruct composée de l'entier score et des cases jouées encodées
 * dans un long
 *
 */
ScoreStruct StrategieMinMax::minimax(int profondeur, bool joueur2,
                                     Plateau* plateau,
                                     uint64_t cases_base_largeur, int niveau) {
  // si la profondeur est 0
  if (profondeur == 0) {
    // on calcule le score après avoir joué les coups
    return scoreApresCoups(plateau, cases_base_largeur, niveau);
  }
  // si c'est le tour du joueur 2 (le joueur qui cherche à minim)
  if (joueur2) {
    // un score ne peut pas dépasser -1 dans les négatifs
    // donc -100 est une bonne borne
    ScoreStruct scoreMax = {-1000000, 0};
    // pour chaque fils du noeud courant
    for (int i = 0; i < plateau->getLargeur(); i++) {
      // appel récursif à minmax sur le fils (ajoute i à l'entier en base
      // largeur)
      ScoreStruct newScore =
        minimax(profondeur - 1, false, plateau,
                cases_base_largeur +
                  ((uint64_t)i *
                   (uint64_t)(int)(double)(powl(
                     (long double)plateau->getLargeur(), (long double)niveau))),
                niveau + 1);
      //std::cout << "received score at (max) depth " << profondeur << ":"
      //          << printScore(newScore, plateau->getLargeur(), niveau + 1)
      //          << std::endl;
      // garde le score qui est le max;
      if (scoreMax.score < newScore.score &&
          newScore.score != INVALID_FORECAST) {
        scoreMax = newScore;
        // ajoute un peu de hasard pour éviter de rendre la strat prévisible
      } else if (scoreMax.score == newScore.score &&
                 newScore.score != INVALID_FORECAST) {
        if (rand() % 2 == 0) {
          scoreMax = newScore;
        }
      }
    }
    // verifie si on a pas une ligne injouable
    if (scoreMax.score == -1000000)
      return {1000000, cases_base_largeur};
    else
      return scoreMax;
  } else {
    // un score ne peut pas dépasser 100
    // donc 101 est une bonne borne
    ScoreStruct scoreMin = {1000000, 0};
    // pour chaque fils du noeud courant
    for (int i = 0; i < plateau->getLargeur(); i++) {
      // faire le min avec les appels récursifs de la fonction en ajoutant la
      // colonne à visiter à l'entier base largeur
      ScoreStruct newScore =
        minimax(profondeur - 1, true, plateau,
                cases_base_largeur +
                  ((uint64_t)i *
                   (uint64_t)(int)(double)(powl(
                     (long double)plateau->getLargeur(), (long double)niveau))),
                niveau + 1);
      //std::cout << "received score at (min) depth " << profondeur << ":"
      //          << printScore(newScore, plateau->getLargeur(), niveau + 1)
      //          << std::endl;
      // garde le score si il est le min
      if (scoreMin.score > newScore.score &&
          newScore.score != INVALID_FORECAST) {
        scoreMin = newScore;
      // rends la strat moins prévisible
      } else if (scoreMin.score == newScore.score &&
                 newScore.score != INVALID_FORECAST) {
        if (rand() % 2 == 0) {
          scoreMin = newScore;
        }
      }
    }
    // verifie si on a pas une ligne injouable
    if (scoreMin.score == 1000000)
      return {-1000000, cases_base_largeur};
    else
      return scoreMin;
  }
}

std::string StrategieMinMax::printScore(ScoreStruct s, int largeur,
                                        int niveau) {
  // reste de ce qui a été enlevé pour l'instant de cases_base_largeur
  int reste = s.cases_jouees;

  int colonne;

  std::string response = "(";

  // pour chaque niveau
  for (int i = 0; i < niveau; i++) {
    // on recupere l'entier
    // e = reste % largeur**(i+1)
    colonne = reste % (int)(std::pow((float)largeur, (float)(i + 1)));
    // reste = reste - e
    reste -= colonne;
    // e = e/(largeur**i)
    colonne = colonne / ((int)(float)(std::pow((float)largeur, (float)(i))));
    // on enregiste la colonne jouee
    response = response + std::to_string(colonne) + ",";
  }
  response = response + "):" + std::to_string(s.score);

  // on retourne le score
  return response;
}

/**
 * scoreApresCoups: le score après avoir joué les coups
 *
 * @param partie: le plateau de la partie
 * @param cases_base_largeur: les cases à joueur en base largeur
 * @param niveau: le niveau de profondeur auquel joueur
 *
 * @return une ScoreStruct avec le score et cases_base_largeur
 */
ScoreStruct StrategieMinMax::scoreApresCoups(Plateau* partie,
                                             uint64_t cases_base_largeur,
                                             int niveau) {
  // reste de ce qui a été enlevé pour l'instant de cases_base_largeur
  int reste = cases_base_largeur;
  // vector qui stock les nombre extraits pour servir de pile
  std::vector<int> cases_jouees;

  int colonne;

  // pour chaque niveau
  for (int i = 0; i < niveau; i++) {
    // on recupere l'entier
    // e = reste % largeur**(i+1)
    colonne =
      reste % (int)(std::pow((float)partie->getLargeur(), (float)(i + 1)));
    // reste = reste - e
    reste -= colonne;
    // e = e/(largeur**i)
    colonne = colonne /
              ((int)(float)(std::pow((float)partie->getLargeur(), (float)(i))));
    // on joue la colonne e
    bool peutJouer = partie->joue(colonne);
    // si on n'a pas pu jouer car la colonne est pleine
    if (!peutJouer) {
      // on va commencer par restaurer le plateau
      // on parcours le tableau pour retirer les colonnes jouées
      for (int j = 0; j < cases_jouees.size(); j++) {
        partie->retireJeton(cases_jouees[j]);
      }
      // on assigne ensuite un score nul et on retourne
      ScoreStruct response = {INVALID_FORECAST, cases_base_largeur};
      return response;
    } 
    // en cas de victoire de J2 retourner le max et ne pas regarder la suite
    if(partie->gagne(JOUEUR2)) {
      // on sauvegarde la case jouée
      cases_jouees.push_back(colonne);
      // on retire tout ce qui a été joué
      for (int j = 0; j < cases_jouees.size(); j++) {
        partie->retireJeton(cases_jouees[j]);
      }
      // retourne le score max
      ScoreStruct response = {100000, cases_base_largeur};
      return response;
    }
    // en cas de victoire de J1 retourner le min et ne pas regarder la suite
    if(partie->gagne(JOUEUR1)) {
      // on sauvegarde la case jouée
      cases_jouees.push_back(colonne);
      // on retire tout ce qui a été joué
      for (int j = 0; j < cases_jouees.size(); j++) {
        partie->retireJeton(cases_jouees[j]);
      }
      // retourne le score min
      ScoreStruct response = {-100000, cases_base_largeur};
      return response;
    }
    // on enregiste la colonne jouee
    cases_jouees.push_back(colonne);
  }

  // on evalue le score
  int score = partie->score(JOUEUR2);

  // partie->affiche();
  // std::cout << "Score calculé pour (";
  // on parcours le tableau pour retirer les colonnes jouées
  for (int i = 0; i < cases_jouees.size(); i++) {
    // std::cout << cases_jouees[i] << ",";
    partie->retireJeton(cases_jouees[i]);
  }
  // std::cout << "): " << score << std::endl;

  ScoreStruct response = {score, cases_base_largeur};

  // on retourne le score
  return response;
}