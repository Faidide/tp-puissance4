#include "../hpp/Frame.hpp"

#include <wx/dcclient.h>
#include <wx/filedlg.h>
#include <wx/graphics.h>

#include <cstring>
#include <iostream>
#include <sstream>
#include <vector>

MyFrame::MyFrame(const wxString& title, const wxPoint& pos, const wxSize& size)
  : wxFrame(NULL, wxID_ANY, title, pos, size) {
  // crée le menu pour les fichiers
  wxMenu* menuFile = new wxMenu;
  // ajoute l'option pour creer une nouvelle grille
  menuFile->Append(ID_New, "&Nouveau\tCtrl-H", "Creer une nouvelle partie");
  menuFile->AppendSeparator();
  // l'option pour quitter
  menuFile->Append(wxID_EXIT, "&Quitter\tCtrl-Q", "Quitter le logiciel");
  // le menu aide et à propos
  wxMenu* menuHelp = new wxMenu;
  menuHelp->Append(wxID_ABOUT, "&Notre Projet", "Qui sommes nous ?");
  // crée la barre de menu en haut
  wxMenuBar* menuBar = new wxMenuBar;
  // ajoute les précédentes sections
  menuBar->Append(menuFile, "&Fichier");
  menuBar->Append(menuHelp, "&A Propos");
  // initialise le menu
  SetMenuBar(menuBar);
  // crée une barre de status
  CreateStatusBar();
  // initialise la barre de status
  SetStatusText("Bienvenu dans le jeu Puissance 4 !");

  // crée le panel
  wxImage::AddHandler(new wxPNGHandler);
  // y crée les bitmap bouton et trophée
  wxBitmap clear(wxT("clear.png"), wxBITMAP_TYPE_PNG);
  tropheePasDessinable = new wxBitmap(wxT("trophee.png"), wxBITMAP_TYPE_PNG);
  shrugPasDessinable = new wxBitmap(wxT("shrug.png"), wxBITMAP_TYPE_PNG);

  // insère le bitmap bouton dans la topbar
  toolbar = CreateToolBar();
  toolbar->AddTool(ID_Clear, wxT("Effacer"), clear);
  toolbar->Realize();

  // cree le dialogue de nouvelle grille
  newDialog = new NewDialog(wxT("Nouvelle Grille"), this);

  // cree la partie
  partieDuJeu = new Plateau(10, 10, 4);
  // TODO: configurer la partie

  // cree et lance le timer
  timer = new wxTimer(this, TIMER_ID);
  timer->Start(MS_ANIMATION_DELAY);
  running = false;

  // variables pour l'animation
  animationEnCours = false;
  animationColonne = 0;

  // variable pour savoir si on affiche l'écran de fin
  gagnant = -1;

  // type de joueur2
  j2stratID = 0;
  strategieIA = nullptr;

  ecranEgalite = false;
}

// event handler de quand on quitte
void MyFrame::OnExit(wxCommandEvent& event) { Close(true); }

// envent handler du à propos
void MyFrame::OnAbout(wxCommandEvent& event) {
  wxMessageBox("Puissance 4 par Antoine MILLON et Quentin FAIDIDE",
               "Fait pour un projet de L3 MI", wxOK | wxICON_INFORMATION);
}

// event handler de l'appel à une nouvelle grille
void MyFrame::OnNew(wxCommandEvent& event) {
  newDialog->Centre();
  modalID = newDialog->ShowModal();
}

void MyFrame::OnPaint(wxPaintEvent& event) {
  // initialize le dc
  wxPaintDC dc(this);
  // cree le graphicsContext
  wxGraphicsContext* gc = wxGraphicsContext::Create(dc);

  // recup la taille
  double xt, yt;
  gc->GetSize(&xt, &yt);

  taille_x = round(xt);
  taille_y = round(yt);

  int largeur_grille = partieDuJeu->getLargeur() * TAILLE_CELLULE;
  int hauteur_grille = partieDuJeu->getHauteur() * TAILLE_CELLULE;

  // calcule le padding à droite et au dessus pour centrer la grille

  if (hauteur_grille > taille_y) {
    topPadding = 0;
  } else {
    topPadding = (taille_y - hauteur_grille) / 2;
  }

  if (largeur_grille > taille_x) {
    leftPadding = 0;
  } else {
    leftPadding = (taille_x - largeur_grille) / 2;
  }

  // définit les couleurs utilisées
  wxColour bleu, bleuFonce, jaune, jauneFonce, rouge, rougeFonce, vide, blanc;
  bleu.Set(wxT("#425af5"));
  bleuFonce.Set(wxT("#26218a"));
  jaune.Set(wxT("#c4b821"));
  jauneFonce.Set(wxT("#91881c"));
  rouge.Set(wxT("#a3244a"));
  rougeFonce.Set(wxT("#521225"));
  vide.Set(wxT("#2f2f38"));
  blanc.Set(wxT("#aaaaaa"));

  // afficher l'ecran d'égalité si il y a
  if (ecranEgalite) {
    if (gc) {
      // affichage du texte de victoire
      wxFont font(30, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL,
                  wxFONTWEIGHT_BOLD);
      gc->SetFont(font, blanc);
      gc->DrawText(wxT("Égalité"), (taille_x >> 1) - TEXT_DRAW_SHIFT_X,
                   (taille_y >> 1) - TEXT_VICTOIRE_SHIFT_Y);
      // affiche du trophee
      // cree le bitmap de bitmap (quel magnifique design de wxWidget, on est
      // admiratif)
      wxGraphicsBitmap shrugDessinable =
        gc->CreateBitmap(*shrugPasDessinable);
      // recupere la largeur
      int img_largeur = shrugPasDessinable->GetWidth();
      int img_hauteur = shrugPasDessinable->GetHeight();
      gc->DrawBitmap(shrugDessinable, (taille_x >> 1) - (img_largeur >> 1),
                     (taille_y >> 1) - (img_hauteur >> 1), img_largeur,
                     img_hauteur);
      gc->Flush();
    }
    return;
  }

  // affichage de l'écran de fin
  if (gagnant != -1) {
    if (gc) {
      wxString joueur;
      // dessine le nom du joueur gagnant
      if (gagnant == 1) {
        joueur = wxT("Victoire de Joueur 1");
      } else {
        joueur = wxT("Victoire de Joueur 2");
      }
      // affichage du texte de victoire
      wxFont font(30, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL,
                  wxFONTWEIGHT_BOLD);
      gc->SetFont(font, blanc);
      gc->DrawText(joueur, (taille_x >> 1) - TEXT_VICTOIRE_SHIFT_X,
                   (taille_y >> 1) - TEXT_VICTOIRE_SHIFT_Y);
      // affiche du trophee
      // cree le bitmap de bitmap (quel magnifique design de wxWidget, on est
      // admiratif)
      wxGraphicsBitmap tropheeDessinable =
        gc->CreateBitmap(*tropheePasDessinable);
      // recupere la largeur
      int img_largeur = tropheePasDessinable->GetWidth();
      int img_hauteur = tropheePasDessinable->GetHeight();
      gc->DrawBitmap(tropheeDessinable, (taille_x >> 1) - (img_largeur >> 1),
                     (taille_y >> 1) - (img_hauteur >> 1), img_largeur,
                     img_hauteur);
      gc->Flush();
    }
  } else {
    // si ça a marché
    if (gc) {
      // grille de jeu
      dc.GradientFillLinear(
        wxRect(leftPadding - MARGINS, topPadding - MARGINS,
               largeur_grille + (MARGINS * 2), hauteur_grille + (MARGINS * 2)),
        bleu, bleuFonce, wxNORTH);

      for (int i = 0; i < partieDuJeu->getHauteur(); i++) {
        for (int j = 0; j < partieDuJeu->getLargeur(); j++) {
          uint8_t case_courante = partieDuJeu->getGrille(j, i);
          if (case_courante == VIDE) {
            gc->SetPen(*wxBLACK_PEN);
            gc->SetBrush(vide);
            gc->DrawEllipse(leftPadding + (j * TAILLE_CELLULE),
                            topPadding + (i * TAILLE_CELLULE),
                            TAILLE_CELLULE - CELL_MARGIN,
                            TAILLE_CELLULE - CELL_MARGIN);
          } else if (case_courante == ROUGE) {
            gc->SetPen(*wxBLACK_PEN);
            gc->SetBrush(rouge);
            gc->DrawEllipse(leftPadding + (j * TAILLE_CELLULE),
                            topPadding + (i * TAILLE_CELLULE),
                            TAILLE_CELLULE - CELL_MARGIN,
                            TAILLE_CELLULE - CELL_MARGIN);
          } else if (case_courante == JAUNE) {
            gc->SetPen(*wxBLACK_PEN);
            gc->SetBrush(jaune);
            gc->DrawEllipse(leftPadding + (j * TAILLE_CELLULE),
                            topPadding + (i * TAILLE_CELLULE),
                            TAILLE_CELLULE - CELL_MARGIN,
                            TAILLE_CELLULE - CELL_MARGIN);
          } else {
            std::cout << "undefined case: " << case_courante << std::endl;
          }
          gc->Flush();
        }
      }

      // si l'animation est en cours, afficher le pion à sa case
      if (animationEnCours) {
        gc->SetPen(*wxBLACK_PEN);
        // choisis la bonne couleur de pièce
        if (partieDuJeu->getTour() == JOUEUR1) {
          gc->SetBrush(rouge);
        } else {
          gc->SetBrush(jaune);
        }
        // dessine la pièce
        gc->DrawEllipse(leftPadding + (animationColonne * TAILLE_CELLULE),
                        topPadding + (animationStep * TAILLE_CELLULE),
                        TAILLE_CELLULE - CELL_MARGIN,
                        TAILLE_CELLULE - CELL_MARGIN);
      }

      wxString joueur;
      // dessine le nom du joueur à qui c'est le tour
      if (partieDuJeu->getTour() == JOUEUR1) {
        joueur = wxT("Joueur 1");
      } else {
        joueur = wxT("Joueur 2");
      }
      wxFont font(20, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL,
                  wxFONTWEIGHT_BOLD);
      gc->SetFont(font, blanc);
      gc->SetPen(*wxWHITE_PEN);
      gc->SetBrush(*wxWHITE_BRUSH);
      gc->DrawText(joueur, (taille_x >> 1) - TEXT_SHIFT_X,
                   topPadding - TEXT_SHIFT_Y);

      delete gc;
    }
  }
}

void MyFrame::leftClick(wxMouseEvent& event) {
  // si l'animation est en cours, ignorer le clic
  if (animationEnCours) return;
  if (gagnant != -1) return;
  if (ecranEgalite) return;

  // translate coords to ones inside the dc
  wxPoint coords = event.GetPosition();

  // calcule la case dans laquelle le click se situe
  int caseX = (coords.x - leftPadding) / TAILLE_CELLULE;
  int caseY = (coords.y - topPadding) / TAILLE_CELLULE;

  // implement click
  if (caseX >= 0 && caseX < partieDuJeu->getLargeur()) {
    int peutJouer = partieDuJeu->joueTest(caseX);
    if (peutJouer != -1) {
      animationEnCours = true;
      animationColonne = caseX;
      animationLigne = peutJouer;
      animationStep = 0;
    }
  }

  Refresh();
}

void MyFrame::OnTimer(wxTimerEvent& event) {
  if (ecranEgalite) return;

  if (animationEnCours) {
    // nouvelle etape de l'animation
    animationStep++;
    // test de fin de l'animation
    if (animationStep >= (partieDuJeu->getHauteur() - animationLigne)) {
      // désactive l'animation
      animationEnCours = false;
      // joue le jeton
      partieDuJeu->joue(animationColonne);
      // verifie si il y a un gagnant
      if (partieDuJeu->gagne(JOUEUR1)) {
        gagnant = 1;
      } else if (partieDuJeu->gagne(JOUEUR2)) {
        gagnant = 2;
      } else {
        // tester si il y a match nul
        if (partieDuJeu->nbCasesVides() == 0) {
          // on doit afficher l'écran d'égalité
          ecranEgalite = true;
          Refresh();
          return;
        }
        Refresh();
        // si l'ia est activée et si il faut la faire jouer
        if (j2stratID != 0 && partieDuJeu->getTour() == JOUEUR2) {
          int choixIA, hauteurJoue;
          // tant que le choix de l'IA n'est pas valide
          // (normalement pas nécessaire mais soyons prudents)
          do {
            // récupérer un choix de l'IA
            choixIA = strategieIA->choix(partieDuJeu);
            // récupère la hauteur de la piece inseree
            hauteurJoue = partieDuJeu->joueTest(choixIA);
            // todo: remove it
            std::cout << "La trat joue à la hauteur :" << hauteurJoue
                      << std::endl;
            if (hauteurJoue == -1)
              std::cout << "La strat a fait un choix invalide !" << std::endl;
          } while (hauteurJoue == -1);
          // une fois le choix valide, jouer comme on le fait avec le click
          animationEnCours = true;
          animationColonne = choixIA;
          animationLigne = hauteurJoue;
          animationStep = 0;
        }
      }
    }
    Refresh();
  }
  event.Skip();
}

void MyFrame::OnClear(wxCommandEvent& event) {
  // on reset la partie avec les nouvelles dimensions
  animationEnCours = false;
  delete partieDuJeu;
  partieDuJeu =
    new Plateau(newDialog->x_grille, newDialog->y_grille, newDialog->nb_gagne);
  gagnant = -1;
  ecranEgalite = false;
  // on cree la strategie et sauvegarde le paramètre d'IA
  // on ne peut pas utiliser en cours de partie le j2stratID de newDialog car il
  // peut être changé dans le dialogue de nouvelle partie
  j2stratID = newDialog->j2stratID;
  // si on veut jouer contre l'utilisateur
  if (j2stratID == 0) {
    // pas besoin de stratégie
    if (strategieIA != nullptr) {
      delete strategieIA;
    }
    // si on veut jouer contre l'ia hasard, l'instancier
  } else if (j2stratID == 1) {
    // on commence par clear la mémoire
    if (strategieIA != nullptr) {
      delete strategieIA;
    }
    strategieIA = (StrategieBase*)new StrategieHasard();
    // si on veut jouer contre l'ia gloutonne, l'instancier
  } else if (j2stratID == 2) {
    // on commence par clear la mémoire
    if (strategieIA != nullptr) {
      delete strategieIA;
    }
    strategieIA = (StrategieBase*)new StrategieGlouton();
    // si on veut jouer contre l'ia MinMax, l'instancier
  } else if (j2stratID == 3) {
    // on commence par clear la mémoire
    if (strategieIA != nullptr) {
      delete strategieIA;
    }
    strategieIA = (StrategieBase*)new StrategieMinMax();
    // si on veut jouer contre l'ia MinMaxElagage, l'instancier
  } else if (j2stratID == 4) {
    // on commence par clear la mémoire
    if (strategieIA != nullptr) {
      delete strategieIA;
    }
    strategieIA = (StrategieBase*)new StrategieMinMaxElagage();
  }
  Refresh();
  event.Skip();
}

void MyFrame::OnNewOk(wxCommandEvent& event) {
  // on reset la partie avec les nouvelles dimensions
  animationEnCours = false;
  delete partieDuJeu;
  partieDuJeu =
    new Plateau(newDialog->x_grille, newDialog->y_grille, newDialog->nb_gagne);
  gagnant = -1;
  ecranEgalite = false;
  // on cree la strategie et sauvegarde le paramètre d'IA
  // on ne peut pas utiliser en cours de partie le j2stratID de newDialog car il
  // peut être changé dans le dialogue de nouvelle partie
  j2stratID = newDialog->j2stratID;
  // si on veut jouer contre l'utilisateur
  if (j2stratID == 0) {
    // pas besoin de stratégie
    if (strategieIA != nullptr) {
      delete strategieIA;
    }
    // si on veut jouer contre l'ia hasard, l'instancier
  } else if (j2stratID == 1) {
    // on commence par clear la mémoire
    if (strategieIA != nullptr) {
      delete strategieIA;
    }
    strategieIA = (StrategieBase*)new StrategieHasard();
    // si on veut jouer contre l'ia gloutonne, l'instancier
  } else if (j2stratID == 2) {
    // on commence par clear la mémoire
    if (strategieIA != nullptr) {
      delete strategieIA;
    }
    strategieIA = (StrategieBase*)new StrategieGlouton();
    // si on veut jouer contre l'ia MinMax, l'instancier
  } else if (j2stratID == 3) {
    // on commence par clear la mémoire
    if (strategieIA != nullptr) {
      delete strategieIA;
    }
    strategieIA = (StrategieBase*)new StrategieMinMax();
    // si on veut jouer contre l'ia MinMaxElagage, l'instancier
  } else if (j2stratID == 4) {
    // on commence par clear la mémoire
    if (strategieIA != nullptr) {
      delete strategieIA;
    }
    strategieIA = (StrategieBase*)new StrategieMinMaxElagage();
  }
  Refresh();
  event.Skip();
}

void MyFrame::OnResize(wxSizeEvent& event) { Refresh(); }

wxBEGIN_EVENT_TABLE(MyFrame, wxFrame) EVT_MENU(ID_New, MyFrame::OnNew)
  EVT_MENU(wxID_EXIT, MyFrame::OnExit) EVT_MENU(wxID_ABOUT, MyFrame::OnAbout)
    EVT_PAINT(MyFrame::OnPaint) EVT_LEFT_DOWN(MyFrame::leftClick)
      EVT_TIMER(TIMER_ID, MyFrame::OnTimer) EVT_TOOL(ID_Clear, MyFrame::OnClear)
        EVT_BUTTON(ID_New_Ok, MyFrame::OnNewOk) EVT_SIZE(MyFrame::OnResize)
          wxEND_EVENT_TABLE()