#include "../hpp/StrategieGlouton.hpp"

#include <algorithm>

// Constructors/Destructors
//

StrategieGlouton::StrategieGlouton() {}

StrategieGlouton::~StrategieGlouton() {}

/**
 * Choisis une colonne sur laquelle jouer.
 * @return int
 * @param  partie Le plateau sur lequel on doit jouer.
 */
int StrategieGlouton::choix(Plateau* partie) {
  int colonne = -1;

  // score toutes les colonnes
  std::vector<int> scores = scoreToutesColonnes(partie,partie->getTour());

  int maxId = -1;
  // tante que le choix n'est pas valide
  while(partie->joueTest(colonne)==-1) {
    // récupère l'id du max
    maxId = std::distance(scores.begin(),
                       std::max_element(scores.begin(), scores.end()));
    // pour toutes les colonnes égales au max
    std::vector<int> scoresMax;
    for(int i=0;i<scores.size();i++) {
      // les sauvegarder
      if(scores[i]==scores[maxId])scoresMax.push_back(i);
    }
    // choisis au hasard un des elements du tableau
    colonne = scoresMax[std::rand() % scoresMax.size()];
  }

  // retourne le choix
  return colonne;
}