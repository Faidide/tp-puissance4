#include "../hpp/StrategieMinMaxElagage.hpp"

#include <cmath>
// Constructors/Destructors
//

StrategieMinMaxElagage::StrategieMinMaxElagage(int profondeur)
  : StrategieMinMax(profondeur) {}

StrategieMinMaxElagage::StrategieMinMaxElagage() : StrategieMinMax(5) {}

StrategieMinMaxElagage::~StrategieMinMaxElagage() {}

/**
 * Choisis une colonne sur laquelle jouer.
 * @return int
 * @param  partie Plateau de jeu sur lequel ou souhaite jouer.
 */
int StrategieMinMaxElagage::choix(Plateau* partie) {
  // applique l'algorithme
  ScoreStruct res = minimax(profondeur_minmax, true, partie);
  int colonne = res.cases_jouees % partie->getLargeur();
  // si la strat est en pls à cause d'une fin de plateau
  if (partie->joueTest(colonne) == -1 || res.score == -1000000 ||
      res.score == 1000000 || res.score == INVALID_FORECAST) {
    // tester toutes les casses unes à une
    for (int i = 0; partie->getLargeur(); i++) {
      // si la case est jouable, la jouer
      if (partie->joueTest(i) != -1) {
        return i;
      }
    }
  }
  // retourne la colonne sur laquelle jouer si valide
  return colonne;
}

/**
 * minimax algorithm
 *
 * @param profondeur : un entier qui décrit la profondeur à atteindre restante
 * @param joueur2 : le booleen qui dit que c'est le tour du joueur2
 * @param plateau : le pointeur vers le plateau de jeu
 * @param cases_base_largeur : un entier long qui encode
 * en base largeur les choix à appliquer au plateau pour calculer le score
 * @param niveau : le niveau de profondeur atteint pour le moment, sert ajouter
 * à cases_base_largeur
 *
 * @return scorestruct composée de l'entier score et des cases jouées encodées
 * dans un long
 *
 */
ScoreStruct StrategieMinMaxElagage::minimax(int profondeur, bool joueur2,
                                            Plateau* plateau,
                                            uint64_t cases_base_largeur,
                                            int niveau, int alpha, int beta) {
  // si la profondeur est 0
  if (profondeur == 0) {
    // on calcule le score après avoir joué les coups
    return scoreApresCoups(plateau, cases_base_largeur, niveau);
  }
  // si c'est le tour du joueur 2 (le joueur qui cherche à maxim)
  if (joueur2) {
    // un score ne peut pas dépasser -1 dans les négatifs
    // donc -100 est une bonne borne
    ScoreStruct scoreMax = {-1000000, 0};
    // pour chaque fils du noeud courant
    for (int i = 0; i < plateau->getLargeur(); i++) {
      // appel récursif à minmax sur le fils (ajoute i à l'entier en base
      // largeur)
      ScoreStruct newScore =
        minimax(profondeur - 1, false, plateau,
                cases_base_largeur +
                  ((uint64_t)i *
                   (uint64_t)(int)(double)(powl(
                     (long double)plateau->getLargeur(), (long double)niveau))),
                niveau + 1, alpha, beta);
      // garde le score qui est le max;
      if (scoreMax.score < newScore.score &&
          newScore.score != INVALID_FORECAST) {
        scoreMax = newScore;
        // rends la strat moins prévisible
      } else if (scoreMax.score == newScore.score &&
                 newScore.score != INVALID_FORECAST) {
        if (rand() % 2 == 0) {
          scoreMax = newScore;
        }
      }
      // on met a jour alpha
      if (scoreMax.score != -1000000) {
        alpha = std::max(alpha, scoreMax.score);
      }
      // si alpha et beta se croisent on abandonne
      if (beta <= alpha) {
        break;
      }
    }
    // verifie si on a pas une ligne injouable
    if (scoreMax.score == -1000000)
      return {1000000, cases_base_largeur};
    else
      return scoreMax;
  } else {
    // un score ne peut pas dépasser 100
    // donc 101 est une bonne borne
    ScoreStruct scoreMin = {1000000, 0};
    // pour chaque fils du noeud courant
    for (int i = 0; i < plateau->getLargeur(); i++) {
      // faire le min avec les appels récursifs de la fonction en ajoutant la
      // colonne à visiter à l'entier base largeur
      ScoreStruct newScore =
        minimax(profondeur - 1, true, plateau,
                cases_base_largeur +
                  ((uint64_t)i *
                   (uint64_t)(int)(double)(powl(
                     (long double)plateau->getLargeur(), (long double)niveau))),
                niveau + 1, alpha, beta);
      // garde le score si il est le min
      if (scoreMin.score > newScore.score &&
          newScore.score != INVALID_FORECAST) {
        scoreMin = newScore;
        // rends la strat moins prévisible
      } else if (scoreMin.score == newScore.score &&
                 newScore.score != INVALID_FORECAST) {
        if (rand() % 2 == 0) {
          scoreMin = newScore;
        }
      }
      // on met a jour alpha
      if (scoreMin.score != 1000000) {
        beta = std::min(beta, scoreMin.score);
      }
      // si alpha et beta se croisent on abandonne la suite
      if (beta <= alpha) {
        break;
      }
    }
    // verifie si on a pas une ligne injouable
    if (scoreMin.score == 1000000)
      return {-1000000, cases_base_largeur};
    else
      return scoreMin;
  }
}
