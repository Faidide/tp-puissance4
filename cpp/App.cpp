#include "../hpp/App.hpp"
#include "../hpp/Frame.hpp"
#include <cstdlib>
#include <ctime>

bool MyApp::OnInit() {
  // initialise le hasard
  std::srand(std::time(nullptr));
  // cree la frame principale
  MyFrame* frame =
    new MyFrame("Puissance 4", wxPoint(50, 50), wxSize(800, 800));
  // affiche la frame principale
  frame->Show(true);
  return true;
}