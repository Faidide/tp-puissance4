#include "../hpp/StrategieHasard.hpp"

// Constructors/Destructors
//

StrategieHasard::StrategieHasard() {}

StrategieHasard::~StrategieHasard() {}

/**
 * Fait un choix de colonne où jouer au hasard.
 * @return int
 * @param  partie Pointeur vers le plateau du jeu en cours.
 */
int StrategieHasard::choix(Plateau* partie) {
    int colonne = 0;
    do {
        colonne = std::rand() % partie->getLargeur();
    } while (partie->joueTest(colonne)==-1);
    // retourne le choix au hasard
    return colonne;
}