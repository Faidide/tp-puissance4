#include "../hpp/StrategieScoreBase.hpp"

// Constructors/Destructors
//

StrategieScoreBase::StrategieScoreBase() {}

StrategieScoreBase::~StrategieScoreBase() {}

std::vector<int> StrategieScoreBase::scoreToutesColonnes(Plateau* partie, uint8_t joueur) {
  // cree le vector qu'on va retourner
  std::vector<int> mesScores;
  // pour chaque colonne
  for(int i=0; i<partie->getLargeur();i++) {
    // poser un jeton dans la colonne
    int hauteurJeu = partie->joue(i);
    // saute la boucle avec un score nul si on ne peut le poser
    if(hauteurJeu==-1) {
        mesScores.push_back(0);
        continue;
    }
    // calculer le score
    mesScores.push_back(partie->score(joueur));
    // retirer le jeton
    partie->retireJeton(i);
  }
  // retourne le vector avec les résultats
  return mesScores;
}
