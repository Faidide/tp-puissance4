#include "../hpp/NewDialog.hpp"

#include "../hpp/Frame.hpp"

NewDialog::NewDialog(const wxString &title, wxWindow *parent)
  : wxDialog(parent, ID_NEW_DIAG, title, wxDefaultPosition, wxSize(400, 290)) {
  wxPanel *panel = new wxPanel(this, -1);

  wxBoxSizer *vbox = new wxBoxSizer(wxVERTICAL);
  wxBoxSizer *hbox = new wxBoxSizer(wxHORIZONTAL);

  wxStaticBox *st = new wxStaticBox(panel, -1, wxT("Nouvelle Grille"),
                                    wxPoint(5, 5), wxSize(400, 290));

  // champ pour entrer la taille et la longueur gagnante
  tcx =
    new wxSpinCtrl(panel, ID_SpinX, wxT(""), wxPoint(255, 35), wxSize(120, 30),
                   wxTE_PROCESS_ENTER, 3, 100, 10, wxT("Largeur"));
  tcy =
    new wxSpinCtrl(panel, ID_SpinY, wxT(""), wxPoint(255, 85), wxSize(120, 30),
                   wxTE_PROCESS_ENTER, 3, 100, 10, wxT("Hauteur"));
  tcnb =
    new wxSpinCtrl(panel, ID_SpinNB, wxT(""), wxPoint(255, 135), wxSize(120, 30),
                   wxTE_PROCESS_ENTER, 3, 100, 4, wxT("Aligmenent gagnant"));

  // champ pour sélectionner le type de joueur2
  /** identifiants de stratégies
   *
   * 0: joueur 2 est un utilisateur
   * 1: stratégie Hasard
   * 2: stratégie Gloutonne
   * 3: stratégie MinMax
   * 4: stratégie MinMaxÉlagage
   *
   */
  choicePlayer2 = new wxChoice(panel, ID_Joueur2, wxPoint(200, 185),
                               wxSize(175, 30), 0, NULL, wxCB_DROPDOWN);

  choicePlayer2->Append(wxT("Utilisateur"));
  choicePlayer2->Append(wxT("IA Hasard"));
  choicePlayer2->Append(wxT("IA Gloutonne"));
  choicePlayer2->Append(wxT("IA MinMax"));
  choicePlayer2->Append(wxT("IA MinMaxÉlagage"));
  choicePlayer2->SetSelection(0);
  j2stratID = 0;

  // text à côté des champs à entrer
  wxStaticText *txtx = new wxStaticText(panel, -1, wxT("largeur:"),
                                        wxPoint(15, 40), wxSize(140, 50));
  wxStaticText *txty = new wxStaticText(panel, -1, wxT("hauteur:"),
                                        wxPoint(15, 90), wxSize(140, 50));
  // nombre de coups pour gagner
  wxStaticText *txtnb = new wxStaticText(panel, -1, wxT("alignement gagnant:"),
                                         wxPoint(15, 140), wxSize(140, 50));
  // type de joueur2
  wxStaticText *txtplay = new wxStaticText(panel, -1, wxT("joueur 2:"),
                                           wxPoint(15, 190), wxSize(140, 50));

  // boutons
  okButton =
    new wxButton(this, ID_New_Ok, wxT("Ok"), wxDefaultPosition, wxSize(70, 30));
  exitButton = new wxButton(this, ID_New_Close, wxT("Fermer"),
                            wxDefaultPosition, wxSize(70, 30));

  // boites d'arrangement
  hbox->Add(okButton, 1);
  hbox->Add(exitButton, 1, wxLEFT, 5);

  vbox->Add(panel, 1);
  vbox->Add(hbox, 0, wxALIGN_CENTER | wxTOP | wxBOTTOM, 10);

  x_grille = 10;
  y_grille = 10;

  SetSizer(vbox);

  greyedOutOk = false;
}

void NewDialog::OnNewClose(wxCommandEvent &event) {
  event.Skip();
  Hide();
}

void NewDialog::OnNewOk(wxCommandEvent &event) {
  // propage l'event au parent
  GetParent()->GetEventHandler()->ProcessEvent(event);
  event.ResumePropagation(999);
  event.Skip();
  Hide();
}

void NewDialog::OnXUpdate(wxSpinEvent &event) {
  x_grille = tcx->GetValue();
  // verifie ou non si il faut griser le bouton
  if (!greyedOutOk && (x_grille < nb_gagne || y_grille < nb_gagne)) {
    greyedOutOk = true;
    okButton->Enable(false);
  } else if (greyedOutOk && !(x_grille < nb_gagne || y_grille < nb_gagne)) {
    greyedOutOk = false;
    okButton->Enable(true);
  }
}

void NewDialog::OnYUpdate(wxSpinEvent &event) {
  y_grille = tcy->GetValue();
  // verifie ou non si il faut griser le bouton
  if (!greyedOutOk && (x_grille < nb_gagne || y_grille < nb_gagne)) {
    greyedOutOk = true;
    okButton->Enable(false);
  } else if (greyedOutOk && !(x_grille < nb_gagne || y_grille < nb_gagne)) {
    greyedOutOk = false;
    okButton->Enable(true);
  }
}

void NewDialog::OnNbUpdate(wxSpinEvent &event) {
  nb_gagne = tcnb->GetValue();
  // verifie ou non si il faut griser le bouton
  if (!greyedOutOk && (x_grille < nb_gagne || y_grille < nb_gagne)) {
    greyedOutOk = true;
    okButton->Enable(false);
  } else if (greyedOutOk && !(x_grille < nb_gagne || y_grille < nb_gagne)) {
    greyedOutOk = false;
    okButton->Enable(true);
  }
}

// garde la variable avec le type de joueur2 à jour avec le wxChoice
void NewDialog::OnJ2Update(wxCommandEvent &event) {
  j2stratID = choicePlayer2->GetSelection();
}

wxBEGIN_EVENT_TABLE(NewDialog, wxDialog)
  EVT_BUTTON(ID_New_Ok, NewDialog::OnNewOk)
    EVT_BUTTON(ID_New_Close, NewDialog::OnNewClose)
      EVT_SPINCTRL(ID_SpinX, NewDialog::OnXUpdate)
        EVT_SPINCTRL(ID_SpinY, NewDialog::OnYUpdate)
          EVT_SPINCTRL(ID_SpinNB, NewDialog::OnNbUpdate)
            EVT_CHOICE(ID_Joueur2, NewDialog::OnJ2Update) wxEND_EVENT_TABLE()