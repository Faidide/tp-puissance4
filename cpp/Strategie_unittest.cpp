#include <limits.h>

#include <chrono>
#include <vector>

#include "../hpp/Plateau.hpp"
#include "../hpp/StrategieGlouton.hpp"
#include "../hpp/StrategieHasard.hpp"
#include "../hpp/StrategieMinMaxElagage.hpp"
#include "gtest/gtest.h"
namespace {

TEST(Strategie, creationHasard) {
  try {
    Plateau plat(10, 10, 5);
    StrategieHasard hasard;
    // ajoute des jetons
    plat.joue(5);
    plat.joue(1);
    SUCCEED();
  } catch (std::exception& e) {
    FAIL() << e.what();
  }
}

TEST(Strategie, jeuHasard) {
  try {
    Plateau plat(10, 5, 3);
    StrategieHasard hasard;
    // ajoute des jetons
    plat.joue(hasard.choix(&plat));
    plat.joue(hasard.choix(&plat));
    plat.joue(hasard.choix(&plat));
    plat.joue(hasard.choix(&plat));
    plat.joue(hasard.choix(&plat));
    plat.joue(hasard.choix(&plat));
    plat.joue(hasard.choix(&plat));
    plat.joue(hasard.choix(&plat));
    plat.affiche();
    SUCCEED();
  } catch (std::exception& e) {
    FAIL() << e.what();
  }
}

TEST(Strategie, jeuGlouton) {
  try {
    Plateau plat(10, 5, 3);
    StrategieGlouton glouton;
    // ajoute des jetons
    plat.joue(glouton.choix(&plat));
    plat.joue(glouton.choix(&plat));
    plat.joue(glouton.choix(&plat));
    plat.joue(glouton.choix(&plat));
    plat.joue(glouton.choix(&plat));
    plat.joue(glouton.choix(&plat));
    plat.joue(glouton.choix(&plat));
    plat.joue(glouton.choix(&plat));
    plat.affiche();
    SUCCEED();
  } catch (std::exception& e) {
    FAIL() << e.what();
  }
}

TEST(Strategie, jeuMinMax) {
  try {
    Plateau plat(10, 5, 3);
    StrategieMinMax minmax;
    // ajoute des jetons
    plat.joue(minmax.choix(&plat));
    plat.joue(minmax.choix(&plat));
    plat.joue(minmax.choix(&plat));
    plat.joue(minmax.choix(&plat));
    plat.joue(minmax.choix(&plat));
    plat.joue(minmax.choix(&plat));
    plat.joue(minmax.choix(&plat));
    plat.joue(minmax.choix(&plat));
    plat.affiche();
    SUCCEED();
  } catch (std::exception& e) {
    FAIL() << e.what();
  }
}

TEST(Strategie, jeuMinMaxElag) {
  try {
    Plateau plat(10, 5, 3);
    StrategieMinMaxElagage minmax;
    // ajoute des jetons
    plat.joue(minmax.choix(&plat));
    plat.joue(minmax.choix(&plat));
    plat.joue(minmax.choix(&plat));
    plat.joue(minmax.choix(&plat));
    plat.joue(minmax.choix(&plat));
    plat.joue(minmax.choix(&plat));
    plat.joue(minmax.choix(&plat));
    plat.joue(minmax.choix(&plat));
    plat.affiche();
    SUCCEED();
  } catch (std::exception& e) {
    FAIL() << e.what();
  }
}

TEST(Strategie, timeMinMax) {
  try {
    // cree le plateau
    Plateau plat(7, 6, 4);

    // ajoute des jetons
    plat.joue(1);
    plat.joue(4);
    plat.joue(5);

    // on va utiliser la libraire chrono de std
    using std::chrono::duration;
    using std::chrono::duration_cast;
    using std::chrono::high_resolution_clock;
    using std::chrono::milliseconds;

    for (int i = 1; i <= 5; i++) {
      // cree la strategie
      StrategieMinMax minmax(i);
      auto t1 = high_resolution_clock::now();
      minmax.choix(&plat);
      auto t2 = high_resolution_clock::now();
      auto ms_int = duration_cast<milliseconds>(t2 - t1);
      std::cout << "Depth: " << i << ", Time(ms):" << (int)ms_int.count()
                << std::endl;
    }

    SUCCEED();
  } catch (std::exception& e) {
    FAIL() << e.what();
  }
}

TEST(Strategie, timeMinMaxElagage) {
  try {
    // cree le plateau
    Plateau plat(7, 6, 4);

    // ajoute des jetons
    plat.joue(1);
    plat.joue(4);
    plat.joue(5);

    // on va utiliser la libraire chrono de std
    using std::chrono::duration;
    using std::chrono::duration_cast;
    using std::chrono::high_resolution_clock;
    using std::chrono::milliseconds;

    for (int i = 1; i <= 6; i++) {
      // cree la strategie
      StrategieMinMaxElagage minmax(i);
      auto t1 = high_resolution_clock::now();
      minmax.choix(&plat);
      auto t2 = high_resolution_clock::now();
      auto ms_int = duration_cast<milliseconds>(t2 - t1);
      std::cout << "Depth: " << i << ", Time(ms):" << (int)ms_int.count()
                << std::endl;
    }

    SUCCEED();
  } catch (std::exception& e) {
    FAIL() << e.what();
  }
}

}  // namespace