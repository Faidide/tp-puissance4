#include <iostream>
#include "gtest/gtest.h"
#include <cstdlib>
#include <ctime>

int main(int argc, char **argv)
{
    std::srand(std::time(nullptr));
    ::testing::InitGoogleTest(&argc, argv);
    std::cout << "LANCEMENT DES TESTS ..." << std::endl;
    int ret{RUN_ALL_TESTS()};
    if (!ret)
        std::cout << "Test passés avec succès" << std::endl;
    else
        std::cout << "Échec des tests" << std::endl;
    return 0;
}