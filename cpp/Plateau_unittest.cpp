#include "../hpp/Plateau.hpp"

#include <limits.h>

#include "gtest/gtest.h"
namespace {

TEST(Plateau, affiche) {
  try {
    Plateau plat(10, 10, 5);
    // ajoute des jetons
    plat.joue(5);
    plat.joue(1);
    plat.joue(2);
    plat.joue(5);
    plat.joue(5);
    plat.joue(2);
    plat.joue(5);
    plat.joue(3);
    plat.joue(5);
    plat.joue(4);
    plat.joue(9);
    plat.affiche();
    SUCCEED();
  } catch (std::exception& e) {
    FAIL() << e.what();
  }
}

TEST(Plateau, Creation1) {
  Plateau plat;
  EXPECT_EQ(plat.dansGrille(7, 6), false);
  EXPECT_EQ(plat.dansGrille(6, 5), true);
  EXPECT_EQ(plat.dansGrille(5, 6), false);
  SUCCEED();
}

TEST(Plateau, Creation2) { 
  Plateau plat(25, 45);
  EXPECT_EQ(plat.dansGrille(25, 45), false);
  EXPECT_EQ(plat.dansGrille(24, 44), true);
  EXPECT_EQ(plat.dansGrille(40, 6), false);
  SUCCEED();
}

TEST(Plateau, Creation3) {
  try {
    Plateau plat(25, -45);
    FAIL();
  } catch (std::exception& e) {
    EXPECT_EQ(std::string(e.what()).compare("Taille de plateau invalide"), 0);
  }
}

TEST(Plateau, Creation4) {
  try {
    Plateau plat(25, 0);
    FAIL();
  } catch (std::exception& e) {
    EXPECT_EQ(std::string(e.what()).compare("Taille de plateau invalide"), 0);
  }
}

TEST(Plateau, Creation5) {
  try {
    Plateau plat(25, 10, 0);
    FAIL();
  } catch (std::exception& e) {
    EXPECT_EQ(std::string(e.what()).compare("Taille de combinaison gagnante invalide"), 0);
  }
}

TEST(Plateau, Creation6) {
  try {
    Plateau plat(25, 10, 1);
    FAIL();
  } catch (std::exception& e) {
    EXPECT_EQ(std::string(e.what()).compare("Taille de combinaison gagnante invalide"), 0);
  }
}

TEST(Plateau, Creation7) {
  Plateau plat(25, 10);
  SUCCEED();
}

TEST(Plateau, Creation8) {
  Plateau plat(25, 10, 4);
  SUCCEED();
}

TEST(Plateau, CreationLong) {
  try {
    Plateau plat(25, 190, 4);
    for(int i=0; i<plat.getLargeur();i++) {
      for(int j=0; j<plat.getHauteur();j++) {
        if(plat.getGrille(i,j)!=VIDE) {
          FAIL() << "Case ("<< i << "," << j <<") invalide !";
        }
      }
    }
    SUCCEED();
  } catch(std::exception &err) {
    FAIL() << err.what();
  }
}

TEST(Plateau, Creation9) {
  try {
    Plateau plat(6, 6, 10);
    FAIL();
  } catch (std::exception& e) {
    EXPECT_EQ(std::string(e.what()).compare("Taille de plateau invalide"), 0);
  }
}

TEST(Plateau, GagneHorizontale1) {
  try {
    Plateau plat(10, 10, 5);
    // ajoute des jetons à la verticale pr joueur1
    plat.joue(0);
    plat.joue(9);
    plat.joue(1);
    plat.joue(9);
    plat.joue(2);
    plat.joue(8);
    plat.joue(3);
    plat.joue(9);
    plat.joue(4);
    plat.joue(9);
    EXPECT_EQ(plat.gagne(JOUEUR1), true);
    EXPECT_EQ(plat.gagne(JOUEUR2), false);
  } catch (std::exception& e) {
    FAIL() << e.what();
  }
}

TEST(Plateau, GagneHorizontale2) {
  try {
    Plateau plat(10, 10, 5);
    // ajoute des jetons à la verticale pr joueur1
    plat.joue(0);
    plat.joue(9);

    plat.joue(1);
    plat.joue(9);

    plat.joue(8);
    plat.joue(2);

    plat.joue(3);
    plat.joue(9);

    plat.joue(4);
    plat.joue(9);

    plat.joue(9);
    plat.joue(0);

    plat.joue(9);

    plat.joue(0);
    plat.joue(6);

    plat.joue(1);
    plat.joue(7);

    plat.joue(2);
    plat.joue(7);

    plat.joue(3);
    plat.joue(7);

    plat.joue(4);
    plat.joue(7);

    EXPECT_EQ(plat.gagne(JOUEUR2), true);
  } catch (std::exception& e) {
    FAIL() << e.what();
  }
}

TEST(Plateau, GagneHorizontale3) {
  try {
    Plateau plat(10, 10, 5);
    // ajoute des jetons à la verticale pr joueur1
    plat.joue(0);
    plat.joue(9);

    plat.joue(1);
    plat.joue(6);

    plat.joue(8);
    plat.joue(2);

    plat.joue(3);
    plat.joue(6);

    plat.joue(4);
    plat.joue(9);

    plat.joue(9);
    plat.joue(0);

    plat.joue(9);

    plat.joue(0);
    plat.joue(6);

    plat.joue(1);
    plat.joue(7);

    plat.joue(7);
    plat.joue(2);

    plat.joue(3);
    plat.joue(7);

    plat.joue(4);
    plat.joue(7);

    EXPECT_EQ(plat.gagne(JOUEUR2), false);
    EXPECT_EQ(plat.gagne(JOUEUR1), false);
  } catch (std::exception& e) {
    FAIL() << e.what();
  }
}

TEST(Plateau, GagneHorizontale4) {
  try {
    Plateau plat(10, 10, 5);
    // ajoute des jetons à la verticale pr joueur1
    plat.joue(0);
    plat.joue(9);
    plat.joue(1);
    plat.joue(1);
    plat.joue(9);
    plat.joue(2);
    plat.joue(8);
    plat.joue(3);
    plat.joue(9);
    plat.joue(4);
    plat.joue(9);
    EXPECT_EQ(plat.gagne(JOUEUR1), false);
  } catch (std::exception& e) {
    FAIL() << e.what();
  }
}

TEST(Plateau, GagneVerticale1) {
  try {
    Plateau plat(10, 10, 5);
    // ajoute des jetons
    plat.joue(5);
    plat.joue(1);
    plat.joue(5);
    plat.joue(2);
    plat.joue(5);
    plat.joue(2);
    plat.joue(5);
    plat.joue(3);
    plat.joue(5);
    plat.joue(4);
    plat.joue(9);
    EXPECT_EQ(plat.gagne(JOUEUR1), true);
    EXPECT_EQ(plat.gagne(JOUEUR2), false);
  } catch (std::exception& e) {
    FAIL() << e.what();
  }
}

TEST(Plateau, GagneVerticale2) {
  try {
    Plateau plat(10, 10, 5);
    // ajoute des jetons
    plat.joue(5);
    plat.joue(1);
    plat.joue(2);
    plat.joue(5);
    plat.joue(5);
    plat.joue(2);
    plat.joue(5);
    plat.joue(3);
    plat.joue(5);
    plat.joue(4);
    plat.joue(9);
    EXPECT_EQ(plat.gagne(JOUEUR1), false);
    EXPECT_EQ(plat.gagne(JOUEUR2), false);
  } catch (std::exception& e) {
    FAIL() << e.what();
  }
}

TEST(Plateau, getGrille) {
  try {
    Plateau plat(10, 10, 5);
    // ajoute des jetons
    plat.joue(5);
    plat.joue(1);
    plat.joue(2);
    plat.joue(5);
    plat.joue(5);
    plat.joue(2);
    plat.joue(5);
    plat.joue(3);
    plat.joue(5);
    plat.joue(4);
    plat.joue(9);
    EXPECT_EQ(plat.getGrille(5,9), JOUEUR1);
    EXPECT_EQ(plat.getGrille(5,8), JOUEUR2);
    EXPECT_EQ(plat.getGrille(5,5), JOUEUR1);
    EXPECT_EQ(plat.getGrille(5,4), VIDE);
  } catch (std::exception& e) {
    FAIL() << e.what();
  }
}

TEST(Plateau, retireJeton1) {
  try {
    Plateau plat(10, 10, 5);
    // ajoute des jetons
    plat.joue(5);
    plat.joue(1);
    plat.joue(2);
    EXPECT_EQ(plat.getGrille(5,9), JOUEUR1);
    plat.affiche();
    plat.retireJeton(5);
    plat.affiche();
    EXPECT_EQ(plat.getGrille(5,9), VIDE);
  } catch (std::exception& e) {
    FAIL() << e.what();
  }
}


}  // namespace