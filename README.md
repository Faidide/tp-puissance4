# Puissance 4
Par Antoine MILLON et Quentin FAIDIDE.


Le makefile est assez modulable. 


Vous pouvez:
- Compiler le programme main avec wxWidget (nécessite wxWidget)

Et pour les tests vous avez comme option:
- Compiler les tests avec Google Test (nécessite Google Test)
- Compiler dans un contenaire (ne nécessite rien, marche partout)

## Compiler le programme main avec wxWidget
### Installer wxWidgets
```bash
sudo apt-get update
sudo apt-get install libwxgtk2.8-dev libwxgtk2.8-dbg
sudo ln -sv wx-2.8/wx wx
```

### Apeller le makefile
```bash
make main
```

### Lancer le programme
```bash
./main 
```

## Compiler et lancer les tests dans un container
```bash
# utilise le Dockerfile pour construire un debian avec wxWidgets et google tests
sudo docker build . -t puissance4
# lance les tests à l'intérieur du container
sudo docker run -it puissance4 ./main_test
```

## Compiler les tests avec Google Test
Téléchargez et compilez la dernière version de google test:
```bash
apt-get -qq update \
    && apt-get -qq install --no-install-recommends cmake autoconf libwxgtk3.0-dev \
    && ln -sv wx-3.0/wx wx
git clone --depth=1 -b master https://github.com/google/googletest.git
mkdir googletest/build
cd googletest/build
cmake ..
make
sudo make install
```

```bash
export GTEST_PATH=/usr/local/lib/libgtest.a && make main_test
./main_test
```

## Features
- Jeu à plusieurs
- Interface wxWidgets
- Jouer contre l'ordinateur avec différentes stratégies

## Cycle de vie du logiciel
- Envoie des modifications au gestionnaire de version
- Construction si nécessaire d'un environnement de compilation avec .meta/Dockerfile et mise à dispotion dans un registre d'images
- Tests de compilation
- Tests unitaires
- Génération du tag de version avec semantic-release (angular commit message format)
- Génération de la release avec les downloads