import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

# read the data
bench_data = pd.read_csv("results_minmax_benchmark2.csv", header=None)

# plot it
x = bench_data[0]
y = bench_data[1]/1000

plt.plot(x,y, "g", label="minmax elagage runtime")
plt.grid()
plt.title("Temps de calcul de minmax avec élagage")
plt.legend()
plt.ylabel("time (sec)")
plt.xlabel("depth")
plt.savefig("output2.jpg")
