import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

# read the data
bench_data = pd.read_csv("results_minmax_benchmark.csv", header=None)

# plot it
x = bench_data[0]
y = bench_data[1]
y2 = bench_data[2]

plt.plot(x,y, "r", label="minmax runtime")
plt.plot(x,y2, "g", label="minmax elagage runtime")
plt.grid()
plt.title("Temps de calcul des minmax avec ou sans élagage")
plt.legend()
plt.ylabel("time (ms)")
plt.xlabel("depth")
plt.savefig("output.jpg")
