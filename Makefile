CXX = g++
CXXFLAGS = -std=c++17 -Wall -I h -I /usr/local/include/gtest/ -c
LXXFLAGS = -std=c++17 -I h -pthread
OBJECTS = ./obj/Plateau.o ./obj/main.o ./obj/Plateau_unittest.o
COMPILOPTS = -Wall -g -ggdb3
GTEST_PATH ?= /usr/lib/libgtest.so
ALL = main main_test
STRATEGY_SRC = ./cpp/StrategieHasard.cpp ./cpp/StrategieScoreBase.cpp ./cpp/StrategieGlouton.cpp ./cpp/StrategieMinMax.cpp ./cpp/StrategieMinMaxElagage.cpp ./cpp/StrategieBase.cpp
STRATEGY_OBJ = ./obj/StrategieHasard.o ./obj/StrategieScoreBase.o ./obj/StrategieGlouton.o ./obj/StrategieMinMax.o ./obj/StrategieMinMaxElagage.o ./obj/StrategieBase.o

all: $(ALL)

# LINKING
main_test: ./obj/Plateau.o ./obj/test_main.o ./obj/Plateau_unittest.o $(STRATEGY_OBJ)  ./obj/Strategie_unittest.o
	$(CXX) $(LXXFLAGS) $(COMPILOPTS) -o main_test ./obj/Plateau.o $(STRATEGY_OBJ) ./obj/test_main.o ./obj/Plateau_unittest.o ./obj/Strategie_unittest.o $(GTEST_PATH)

main: ./cpp/main.cpp ./cpp/App.cpp ./cpp/NewDialog.cpp ./cpp/Frame.cpp ./cpp/Plateau.cpp $(STRATEGY_SRC)
	$(CXX) $^ -o $@ $(COMPILOPTS) `wx-config --libs` `wx-config --cxxflags`

# OBJECT COMPILING
./obj/Plateau.o: ./cpp/Plateau.cpp
	$(CXX) $(CXXFLAGS) $(COMPILOPTS) ./cpp/Plateau.cpp -o ./obj/Plateau.o

./obj/StrategieHasard.o: ./cpp/StrategieHasard.cpp 
	$(CXX)  $(COMPILOPTS) $(CXXFLAGS) ./cpp/StrategieHasard.cpp -o ./obj/StrategieHasard.o

./obj/StrategieBase.o: ./cpp/StrategieBase.cpp 
	$(CXX)  $(COMPILOPTS) $(CXXFLAGS) ./cpp/StrategieBase.cpp -o ./obj/StrategieBase.o

./obj/StrategieScoreBase.o: ./cpp/StrategieScoreBase.cpp
	$(CXX)  $(COMPILOPTS) $(CXXFLAGS) ./cpp/StrategieScoreBase.cpp -o ./obj/StrategieScoreBase.o

./obj/StrategieGlouton.o: ./cpp/StrategieGlouton.cpp
	$(CXX)  $(COMPILOPTS) $(CXXFLAGS) ./cpp/StrategieGlouton.cpp -o ./obj/StrategieGlouton.o

./obj/StrategieMinMax.o: ./cpp/StrategieMinMax.cpp
	$(CXX)  $(COMPILOPTS) $(CXXFLAGS) ./cpp/StrategieMinMax.cpp -o ./obj/StrategieMinMax.o

./obj/StrategieMinMaxElagage.o: ./cpp/StrategieMinMaxElagage.cpp
	$(CXX)  $(COMPILOPTS) $(CXXFLAGS) ./cpp/StrategieMinMaxElagage.cpp -o ./obj/StrategieMinMaxElagage.o

./obj/Plateau_unittest.o: ./cpp/Plateau_unittest.cpp
	$(CXX) $(CXXFLAGS) $(COMPILOPTS) ./cpp/Plateau_unittest.cpp -o ./obj/Plateau_unittest.o

./obj/Strategie_unittest.o: ./cpp/Strategie_unittest.cpp
	$(CXX) $(CXXFLAGS) $(COMPILOPTS) ./cpp/Strategie_unittest.cpp -o ./obj/Strategie_unittest.o

./obj/test_main.o: ./cpp/test_main.cpp
	$(CXX) $(CXXFLAGS) ./cpp/test_main.cpp -o ./obj/test_main.o

# UTILITARIES
clean:
	rm -fv $(ALL) $(OBJECTS)