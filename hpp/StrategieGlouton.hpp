
#ifndef STRATEGIEGLOUTON_H
#define STRATEGIEGLOUTON_H

#include "StrategieScoreBase.hpp"

/**
  * class StrategieGlouton
  * 
  * Stratégie de score qui maximise le score à portée de 1 coup
  *
  */

class StrategieGlouton: public StrategieScoreBase
{
public:

  /**
   * Empty Constructor
   */
  StrategieGlouton();

  /**
   * Empty Destructor
   */
  virtual ~StrategieGlouton();

  /**
   * Choisis une colonne sur laquelle jouer.
   * @return int
   * @param  partie Le plateau sur lequel on doit jouer.
   */
  int choix(Plateau* partie) override final;

private:
  // eventuels attributs privés
};

#endif // STRATEGIEGLOUTON_H
