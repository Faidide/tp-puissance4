
#ifndef STRATEGIEMINMAX_H
#define STRATEGIEMINMAX_H

// value for an invalid forecast
#define INVALID_FORECAST 1345679

#include <cstdint>

/**
 * Structure qui garde le score et les cases jouées
 *
 */
struct ScoreStruct {
  int score;
  uint64_t cases_jouees;
};

#include "StrategieScoreBase.hpp"

/**
 * class StrategieMinMax
 *
 * Classe qui implemente un strategie de score min max.
 *
 */

class StrategieMinMax : public StrategieScoreBase {
 public:
  /**
   * Constructor
   */
  StrategieMinMax();
  StrategieMinMax(int profondeur);

  /**
   * Empty Destructor
   */
  virtual ~StrategieMinMax();

  /**
   * Choisis une colonne sur laquelle jouer.
   * @return int
   * @param  partie Plateau de jeu sur lequel ou souhaite jouer.
   */
  virtual int choix(Plateau* partie) override;

 protected:
  /**
   * scoreApresCoups: le score après avoir joué les coups
   *
   * @param partie: le plateau de la partie
   * @param cases_base_largeur: les cases à joueur en base largeur
   * @param niveau: le niveau de profondeur auquel joueur
   *
   * @return une ScoreStruct avec le score et cases_base_largeur
   */
  ScoreStruct scoreApresCoups(Plateau* partie, uint64_t cases_base_largeur,
                              int niveau);

  /**
   * minimax algorithm
   *
   * @param profondeur : un entier qui décrit la profondeur à atteindre restante
   * @param joueur2 : le booleen qui dit que c'est le tour du joueur2
   * @param plateau : le pointeur vers le plateau de jeu
   * @param cases_base_largeur : un entier long qui encode
   * en base largeur les choix à appliquer au plateau pour calculer le score
   * @param niveau : le niveau de profondeur atteint pour le moment, sert
   * ajouter à cases_base_largeur
   *
   * @return scorestruct composée de l'entier score et des cases jouées encodées
   * dans un long
   *
   */
  virtual ScoreStruct minimax(int profondeur, bool joueur2, Plateau* plateau,
                      uint64_t cases_base_largeur = 0, int niveau = 0);

  std::string printScore(ScoreStruct, int, int);

  // la profondeur par defaut
  int profondeur_minmax;
};

#endif  // STRATEGIEMINMAX_H
