#ifndef PLATEAU_HPP
#define PLATEAU_HPP
#include <inttypes.h>
#include <stdlib.h>
#include <string.h>

#include <exception>
#include <iostream>

// identifiants pour les joueurs
#define JOUEUR1 1
#define JOUEUR2 2

// identifiants pour les jetons
#define VIDE 0
#define ROUGE JOUEUR1  // Couleur du JOUEUR 1
#define JAUNE JOUEUR2  // Couleur du JOUEUR 2

// taille maximale d'une grille
#define DIM_MAX 300

class Plateau {
 private:
  // JOUEUR 1 = JOUEUR1 et JOUEUR 2 = JOUEUR2
  // Cet attribut nous sert a savoir a qui c'est le tour de jouer, cellui-ci
  // vaut soit JOUEUR1 soit JOUEUR2
  uint8_t tour;
  // Un pointeur vers une zone memoire de taille TAILLE_GRILLE_LARGEUR *
  // TAILLE_GRILLE_HAUTEUR Le coin en haut (resp. bas) a gauche de la grille du
  // jeu de puissance 4 correspond a la ligne 0 (resp. TAILLE_GRILLE_HAUTEUR -
  // 1) et la colonne 0 (resp. 0) de notre grille Les elements de notre grille
  // sont appele des jetons.
  uint8_t* grille;
  // Le ieme element du tableau correspond a la hauteur (en partant du bas) du
  // plus bas jeton (en partant du bas) VIDE de la colonne i de la grille Au
  // depart (la grille est vide) notre tableau est initialise a 0
  int* hauteur_remplissement;
  // hauteur max de la grille a tout instant
  int hauteur_max;
  // Largeur de la grille
  int largeur;
  // Hauteur de la grille
  int hauteur;
  // Nombre nécessaire de jeton de la même couleur a la suite pour gagner
  int nombre_a_la_suite;
  // setter de la grille
  void setGrille(int, int, uint8_t);
  // fonctions qui aident à détecter les victoires
  bool gagne_horizontale(uint8_t joueur) const;
  bool gagne_verticale(uint8_t joueur) const;
  bool gagne_diagonale(uint8_t joueur) const;
  // fonction qui aident à calculer le score
  int scoreHorizontale(uint8_t joueur) const;
  int scoreVerticale(uint8_t joueur) const;
  int scoreDiagonale(uint8_t joueur) const;
  bool ligneEstTerminableVerticalement(int x, int y, uint8_t joueur) const;
  bool ligneEstTerminableHorizontalement(int x, int y, uint8_t joueur) const;

 public:
  // Constructeur
  Plateau();
  Plateau(int, int);       // largeur et hauteur en argument
  Plateau(int, int, int);  // largeur, hauteur et nombre_a_la_suite en argument
  // Destructeur
  ~Plateau();
  // accesseur de la grille
  uint8_t getGrille(int, int) const;
  // verifie rapidement que les coordonées son valides
  bool dansGrille(int, int) const;
  // affiche la configuration de la grille sur la sortie standard
  void affiche() const;
  // modifie le plateau pour que le joueur courant ait joue dans la colonne
  // passe en argument, renvoie true si le coup est autorisé false sinon (on ne
  // peut jouer sur une colonne pleine)
  bool joue(int);
  // test si on peut joueur sur cette colonne (renvoie la hauteur de ligne ou
  // -1)
  int joueTest(int);
  // indique si le joueur passe en argument a gagne
  bool gagne(uint8_t) const;
  // pour récupérer la hauteur et largeur de la grille
  int getHauteur() const;
  int getLargeur() const;
  // renvoie le joueur à qui c'est le tour
  uint8_t getTour() const;
  // retire un jeton du plateau
  bool retireJeton(int);
  // calcule le score du plateau pour le joueur
  // le score est la ratio entre la plus longue ligne atteinte et la ligne
  // nécessaire pour gagner multiplié par 100 et casté en int
  // améliorations possibles: ignorer les longues lignes impossibles à terminer
  int score(uint8_t) const;
  // nombre de cases vides
  int nbCasesVides() const;
};

#endif  // PLATEAU_HPP