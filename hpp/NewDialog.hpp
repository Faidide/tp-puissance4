#ifndef DEF_NEWDIAL_HPP
#define DEF_NEWDIAL_HPP

#include <wx/wx.h>
#include <wx/spinctrl.h>

class NewDialog : public wxDialog {
 public:
  NewDialog(const wxString& title, wxWindow* parent);
  wxButton* exitButton;
  wxButton *okButton;
  void OnNewClose(wxCommandEvent& event);
  void OnNewOk(wxCommandEvent& event);
  void OnXUpdate(wxSpinEvent& event);
  void OnYUpdate(wxSpinEvent& event);
  void OnNbUpdate(wxSpinEvent& event);
  void OnJ2Update(wxCommandEvent& event);
  // valueur de taille de grille favorite
  int x_grille = 10;
  int y_grille = 10;
  int nb_gagne = 4;
  wxSpinCtrl *tcx;
  wxSpinCtrl *tcy;
  wxSpinCtrl *tcnb;
  wxChoice *choicePlayer2;
  bool greyedOutOk;
  /** identifiants de stratégies
  *
  * 0: joueur 2 est un utilisateur
  * 1: stratégie Hasard 
  * 2: stratégie Gloutonne
  * 3: stratégie MinMax
  * 4: stratégie MinMaxÉlagage
  *
  */
  int j2stratID;
 private:
  wxWindow* parent;

  wxDECLARE_EVENT_TABLE();
};

enum {
  ID_New_Close = 1002,
  ID_New_Ok = 1003,
  ID_NEW_DIAG = 1005,
  ID_SpinX = 2000,
  ID_SpinY = 2006,
  ID_SpinNB = 2115,
  ID_Joueur2 = 2554
};

#endif  // DEF_NEWDIAL_HPP