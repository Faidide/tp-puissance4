
#ifndef STRATEGIESCOREBASE_H
#define STRATEGIESCOREBASE_H

#include "StrategieBase.hpp"
#include <vector>

/**
  * class StrategieScoreBase
  * 
  */

class StrategieScoreBase: public StrategieBase
{
public:

  /**
   * Empty Constructor
   */
  StrategieScoreBase();

  /**
   * Empty Destructor
   */
  virtual ~StrategieScoreBase();

protected:
  /**
   *
   * calcule le score mais sur toutes les colonnes
   * et retourne un vecteur
   *
   * @param Partie pointeur vers le plateau
   * @param Joueur l'identifiant du joueur qu'on va scorer
   *
   * @return un vecteur des scores pour chaque colonne
   */
  std::vector<int> scoreToutesColonnes(Plateau* partie, uint8_t joueur);
};

#endif // STRATEGIESCOREBASE_H
