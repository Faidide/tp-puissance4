
#ifndef STRATEGIEMINMAXELAGAGE_H
#define STRATEGIEMINMAXELAGAGE_H

#include "StrategieMinMax.hpp"

/**
 * class StrategieMinMaxElagage
 *
 * Strategie qui implémente le min max mais avec élagage
 *
 */

class StrategieMinMaxElagage : public StrategieMinMax {
 public:
  /**
   * Empty Constructor
   */
  StrategieMinMaxElagage();
  StrategieMinMaxElagage(int);

  /**
   * Empty Destructor
   */
  virtual ~StrategieMinMaxElagage();

  /**
   * Choisis une colonne sur laquelle jouer.
   * @return int
   * @param  partie Plateau de jeu sur lequel ou souhaite jouer.
   */
  virtual int choix(Plateau* partie) override final;

 protected:
  /**
   * minimax algorithm
   *
   * @param profondeur : un entier qui décrit la profondeur à atteindre restante
   * @param joueur2 : le booleen qui dit que c'est le tour du joueur2
   * @param plateau : le pointeur vers le plateau de jeu
   * @param cases_base_largeur : un entier long qui encode
   * en base largeur les choix à appliquer au plateau pour calculer le score
   * @param niveau : le niveau de profondeur atteint pour le moment, sert
   * ajouter à cases_base_largeur
   *
   * @return scorestruct composée de l'entier score et des cases jouées encodées
   * dans un long
   *
   */
  ScoreStruct minimax(int profondeur, bool joueur2, Plateau* plateau,
                      uint64_t cases_base_largeur = 0, int niveau = 0,
                      int alpha = -1000000, int beta = 1000000);

 private:
  // Eventuels membres/methodes prives
};

#endif  // STRATEGIEMINMAXELAGAGE_H
