#ifndef DEF_FRAME_HPP
#define DEF_FRAME_HPP

#include <wx/wx.h>
#include "../hpp/NewDialog.hpp"
#include "../hpp/Plateau.hpp"
#include "../hpp/StrategieHasard.hpp"
#include "../hpp/StrategieGlouton.hpp"
#include "../hpp/StrategieMinMaxElagage.hpp"

// taille en pixel d'une case
#define TAILLE_CELLULE 30
// marges entre les cellules
#define CELL_MARGIN 4
// marges en pixels autour de la grille de jeu pour le dessin
#define MARGINS 10
// délai en millisecondes entre chaque frame de l'animation
#define MS_ANIMATION_DELAY 80
// décalage du texte du nom du joueur à qui c'est le tour
#define TEXT_SHIFT_Y 48
#define TEXT_SHIFT_X 63
// décalage du texte de victoire
#define TEXT_VICTOIRE_SHIFT_X 230
#define TEXT_VICTOIRE_SHIFT_Y 250
#define TEXT_DRAW_SHIFT_X 80

class MyFrame : public wxFrame {
 public:
  MyFrame(const wxString& title, const wxPoint& pos, const wxSize& size);
  NewDialog *newDialog;
  wxTimer * timer;
  Plateau *partieDuJeu;
  wxToolBar* toolbar;
  wxBitmap* tropheePasDessinable;
  wxBitmap* shrugPasDessinable;
  StrategieBase* strategieIA;
  bool animationEnCours;
  int animationColonne;
  int animationLigne;
  int animationStep;
  int gagnant;
  bool ecranEgalite;
  /** identifiants de stratégies
  *
  * 0: joueur 2 est un utilisateur
  * 1: stratégie Hasard 
  * 2: stratégie Gloutonne
  * 3: stratégie MinMax
  * 4: stratégie MinMaxÉlagage
  *
  */
  int j2stratID;

 private:
  int modalID;
  bool running;
  void OnNew(wxCommandEvent& event);
  void OnExit(wxCommandEvent& event);
  void OnAbout(wxCommandEvent& event);
  void OnPaint(wxPaintEvent& event);
  void OnClear(wxCommandEvent& event);
  void leftClick(wxMouseEvent& event);
  void OnTimer(wxTimerEvent& event);
  void OnNewOk(wxCommandEvent& event);
  void OnResize(wxSizeEvent& event);

  int taille_x;
  int taille_y;
  int topPadding;
  int leftPadding;
  
  wxDECLARE_EVENT_TABLE();
};

enum { ID_New = 41, ID_Clear = 49, TIMER_ID=51  };

#endif // DEF_FRAME_HPP