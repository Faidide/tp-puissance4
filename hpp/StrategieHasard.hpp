
#ifndef STRATEGIEHASARD_H
#define STRATEGIEHASARD_H

#include "StrategieBase.hpp"

/**
  * class StrategieHasard
  * 
  * Joue sur la première colonne au hasard ou c'est possible. 
  *
  */

class StrategieHasard: public StrategieBase 
{
public:

  /**
   * Empty Constructor
   */
  StrategieHasard();

  /**
   * Empty Destructor
   */
  virtual ~StrategieHasard();

  /**
   * Fait un choix de colonne où jouer au hasard.
   * @return int
   * @param  partie Pointeur vers le plateau du jeu en cours.
   */
  int choix(Plateau* partie) override final;
};

#endif // STRATEGIEHASARD_H
