
#ifndef STRATEGIEBASE_H
#define STRATEGIEBASE_H

#include "Plateau.hpp"
#include <cstdlib>
#include <iostream>
#include <ctime>

/**
  * class StrategieBase
  * 
  * Classe abstraite permettant de faire inférence des stratégies.
  *
  */

class StrategieBase
{
public:

  /**
   * Empty Constructor
   */
  StrategieBase();

  /**
   * Empty Destructor
   */
  virtual ~StrategieBase();

  /**
   * Choisis le pion à joueur.
   * @return int
   * @param  partie Pointeur vers la partie sur laquelle on compte jouer.
   */
  virtual int choix(Plateau* partie) = 0;

};

#endif // STRATEGIEBASE_H
