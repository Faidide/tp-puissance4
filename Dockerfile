# on utilise l'image officielle de gcc (un Debian)
FROM gcc:9.2.0

# Dossier de l'application
WORKDIR /usr/src/app

# Intalle les dépendances nécessaires
RUN apt-get -qq update \
    && apt-get -qq install --no-install-recommends cmake autoconf libwxgtk3.0-dev \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* \
    && ln -sv wx-3.0/wx wx

# Conmpilation de GTest et installation
RUN git clone --depth=1 -b master https://github.com/google/googletest.git
RUN mkdir googletest/build
WORKDIR /usr/src/app/googletest/build
RUN cmake .. \
    && make \
    && make install \
    && rm -rf /usr/src/app/googletest

# cd dans le dossier de l'application
WORKDIR /usr/src/app
# copie des fichiers source et makefile
COPY . .

# compilation de l'application
RUN mkdir obj 
RUN export GTEST_PATH=/usr/local/lib/libgtest.a && make

# lancement de l'application
CMD [ "./main_test" ]